/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trandpl.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import trandpl.dbutil.DBConnection;
import trandpl.pojo.CurrentUser;
import trandpl.pojo.HrPojo;
import trandpl.pojo.UserPojo;

/**
 *
 * @author rriti
 */
public class UserDAO {
    
    public static boolean validateUser(UserPojo user) throws SQLException {
        
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("Select * from users where userid = ? and password =? and type=? and active='Y'");
        
        ps.setString(1,user.getUserid());
        ps.setString(2, user.getPassword());
        ps.setString(3, user.getType());
        
        ResultSet rs = ps.executeQuery();
        
        if(rs.next()){
            CurrentUser.setUserid(rs.getString(1));
            CurrentUser.setId(rs.getString(2));
            CurrentUser.setName(rs.getString(3));
            CurrentUser.setType(rs.getString(5));
            
            
            return true;
        }
        
        return false;
        
    }
    
    public static Map<String ,HrPojo> getAllHrList() throws SQLException{
        
        Connection conn = DBConnection.getConnection();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("Select userid ,id, name from users where type = 'Hr'");
        Map <String , HrPojo> allHr = new HashMap<>();
        
        while(rs.next()){
            String userid = rs.getString(1);
            String id =rs.getString(2);
            String name = rs.getString(3);
            
            HrPojo obj = new HrPojo();
            obj.setHrId(id);
            obj.setHrName(name);
            
            allHr.put(userid ,obj);
        }
       return allHr; 
    }
    
    public static boolean updatePwd(String userid , String password) throws SQLException{
        
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("Update users set password=? , active ='Y' where userid = ?");
        ps.setString(1,password);
        ps.setString(2, userid);
        
        int result = ps.executeUpdate();
        
        if(result==1){
           return true;  
        }
        
        
        return false;
        
    }
    
    public static boolean removeUser(String userId) throws SQLException{
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("Update users set active = 'N' where userid = ?");
        ps.setString(1, userId);
        
        return 1 == ps.executeUpdate();
        
        
    }
    
    public static boolean setJobStatus(String jobId) throws SQLException{
       Connection conn = DBConnection.getConnection();
       PreparedStatement ps = conn.prepareStatement("Update jobs set status = 1 where jobid =?");
       ps.setString(1, jobId);
       
       return 1==ps.executeUpdate();
        
        
    }
    
    public static boolean updateParticipantPassword(String id , String pwd ) throws SQLException {
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("Update users set password =?  where Id =?");
        ps.setString(1, pwd);
        ps.setString(2, id);
        
        return 1==ps.executeUpdate();
        
    }
    
        public static boolean checkOldPasword(String pid ,String oldPwd ) throws SQLException {
        Connection conn = DBConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement("Select * from users where id = ? and password =? ");
        ps.setString(1, pid);
        ps.setString(2, oldPwd);
        ResultSet rs = ps.executeQuery();
        return rs.next();
        
    }
    
}
