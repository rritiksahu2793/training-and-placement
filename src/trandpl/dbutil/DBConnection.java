/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trandpl.dbutil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author rriti
 */
public class DBConnection {
    
    private static Connection conn;
    static{
        try{
            Class.forName("oracle.jdbc.OracleDriver");
            JOptionPane.showMessageDialog(null,"Driver loaded successfully ");
            
            conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "tnp","project");
            JOptionPane.showMessageDialog(null,"Connected to the DB");
            
        }
        catch(ClassNotFoundException ex){
            JOptionPane.showMessageDialog(null,"Error in loading the Driver");
            ex.printStackTrace();
            
        }
        catch(SQLException ex){
             JOptionPane.showMessageDialog(null,"Error connecting to the DB");
             ex.printStackTrace();
        }
        
    }
    
    public static Connection getConnection(){
        return conn;
    }
    
    public static void closeConnection(){
        try{
           conn.close(); 
           JOptionPane.showMessageDialog(null,"disconnected Successfully to the DB");
           
        }
        catch(SQLException ex){
           JOptionPane.showMessageDialog(null,"Error disconnecting to the DB");
           ex.printStackTrace(); 
        }
        
    }
    
}
