/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trandpl.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import trandpl.dao.ParticipantDAO;
import trandpl.dao.UserDAO;
import trandpl.pojo.CurrentUser;
import trandpl.pojo.ParticipantPojo;

/**
 *
 * @author rriti
 */
public class PtEditProfileFrame extends javax.swing.JFrame {

    /**
     * Creates new form PtEditProfileFrame
     */
    private File file;
    private String oldPwd, cnfPwd, npwd;

    public PtEditProfileFrame() {
        initComponents();
        this.setLocationRelativeTo(null);
        resumePanel.setVisible(false);
        passwordPanel.setVisible(false);
        loadData();
    }

    
    
    
    
    private void loadData() {

        try {
            ParticipantPojo pt = ParticipantDAO.getparticipantById(CurrentUser.getId());
            txtName.setText(pt.getName());
            txtPhone.setText(pt.getPhone());
            txtEmail.setText(pt.getUserid());
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error in Connection to DataBase", "DB Error ", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return;

        }

    }

    private boolean validateInputs() {
        if (txtcurrentPwd.getText().trim().isEmpty() || txtNewPwd.getText().trim().isEmpty() || txtCfrmPwd.getText().trim().isEmpty()) {
            return false;
        }
        return true;
    }

    private boolean matchPassword() {

        return (txtNewPwd.getText()).equals(txtCfrmPwd.getText());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtPhone = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtName = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        passwordPanel = new javax.swing.JPanel();
        txtCfrmPwd = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        txtcurrentPwd = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        txtNewPwd = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        chkPassword = new javax.swing.JCheckBox();
        chkResume = new javax.swing.JCheckBox();
        resumePanel = new javax.swing.JPanel();
        txtResume = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/World-Map.png"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, -10, -1, 190));

        jLabel2.setText("Name");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 210, 200, 30));
        jPanel1.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 420, 240, 30));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 410, -1, -1));

        txtPhone.setText("jButton2");
        jPanel1.add(txtPhone, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 340, 250, 30));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 330, -1, -1));
        jPanel1.add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 250, 250, 30));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, -1, -1));

        jLabel6.setText("Phone no");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 300, 150, 30));

        jLabel7.setText("Mail Address");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 380, 160, 30));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 460, 1020, -1));

        passwordPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtCfrmPwd.setText("jButton4");
        passwordPanel.add(txtCfrmPwd, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 140, 250, 30));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        passwordPanel.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 130, -1, 40));

        txtcurrentPwd.setText("jButton4");
        passwordPanel.add(txtcurrentPwd, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 20, 250, 30));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        passwordPanel.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(232, 13, -1, -1));

        txtNewPwd.setText("jButton4");
        txtNewPwd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNewPwdActionPerformed(evt);
            }
        });
        passwordPanel.add(txtNewPwd, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, 250, 30));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        passwordPanel.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(232, 73, -1, -1));

        jLabel11.setText("New Password ");
        passwordPanel.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 73, 185, 42));

        jLabel12.setText("Current Password");
        passwordPanel.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 13, 185, 42));

        jLabel13.setText("Confirm new Password");
        passwordPanel.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 133, 185, 44));

        jPanel1.add(passwordPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 260, 530, 190));

        chkPassword.setText("Want to change the Password ");
        chkPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkPasswordActionPerformed(evt);
            }
        });
        jPanel1.add(chkPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 220, 480, -1));

        chkResume.setText("Want to change Resume");
        chkResume.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkResumeActionPerformed(evt);
            }
        });
        jPanel1.add(chkResume, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 470, 480, 30));

        jButton7.setText("Browse File");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout resumePanelLayout = new javax.swing.GroupLayout(resumePanel);
        resumePanel.setLayout(resumePanelLayout);
        resumePanelLayout.setHorizontalGroup(
            resumePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(resumePanelLayout.createSequentialGroup()
                .addComponent(txtResume, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                .addContainerGap())
        );
        resumePanelLayout.setVerticalGroup(
            resumePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(resumePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(resumePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(resumePanelLayout.createSequentialGroup()
                        .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(6, 6, 6))
                    .addComponent(txtResume, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel1.add(resumePanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 500, 520, 50));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 560, 1020, -1));

        jButton1.setText("Discard Changes");
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 580, -1, -1));

        jButton2.setText("Save Changes ");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 580, -1, -1));

        jButton3.setText("Back");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 580, 140, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1020, 630));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void chkPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkPasswordActionPerformed
        // TODO add your handling code here:
        if (chkPassword.isSelected()) {
            passwordPanel.setVisible(true);
        } else {
            passwordPanel.setVisible(false);
        }
    }//GEN-LAST:event_chkPasswordActionPerformed

    private void chkResumeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkResumeActionPerformed
        // TODO add your handling code here:
        if (chkResume.isSelected()) {
            resumePanel.setVisible(true);
        } else {
            resumePanel.setVisible(false);
        }
    }//GEN-LAST:event_chkResumeActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Select Your Resume");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int result = fileChooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            file = fileChooser.getSelectedFile();
            txtResume.setText(file.getName());
    }//GEN-LAST:event_jButton7ActionPerformed
    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        oldPwd = txtcurrentPwd.getText();
        npwd = txtNewPwd.getText();
        
        boolean pwdChanged = false;
        boolean resumeUpdated = false;
        
        if(chkPassword.isSelected()){
            if(validateInputs()==false){
               JOptionPane.showMessageDialog(null, "Please fill all the fields ", "Empty fields ", JOptionPane.ERROR_MESSAGE);
               return;
            }
            if(matchPassword()==false){
               JOptionPane.showMessageDialog(null, "New password and Confirm Password did not matched ", "Mismatch Password ", JOptionPane.ERROR_MESSAGE);
               return;
            }
            try{
                boolean resp = UserDAO.checkOldPasword(CurrentUser.getId(), oldPwd);
                if(resp==true){
                    UserDAO.updateParticipantPassword(CurrentUser.getId(), npwd);
                    pwdChanged = true;
                }
                else{
                    JOptionPane.showMessageDialog(null, "Please enter correct Password of your account ", "Password Missmatching ", JOptionPane.ERROR_MESSAGE);

                }
            }
            catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error in Connection to DataBase", "DB Error ", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return;

            }
        }
        
        if(chkResume.isSelected()){
            if(txtResume.getText().trim() != ""){
            try{
                ParticipantDAO.updateResume(CurrentUser.getId(), file);
                resumeUpdated = true;
            }
            catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error in Connection to DataBase", "DB Error ", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return;

            }
            catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "File Error ,error in finding file ", "File  Error ", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return;

            }
        }
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txtNewPwdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNewPwdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNewPwdActionPerformed
    
        if(resumeUpdated&&pwdChanged){
            JOptionPane.showMessageDialog(null, "Resume and Password Updated Successfully ", "Success ", JOptionPane.INFORMATION_MESSAGE);

        }
        else if(resumeUpdated){
            JOptionPane.showMessageDialog(null, "Resume Updated Successfully ", "Success ", JOptionPane.INFORMATION_MESSAGE);

        }
         else if(pwdChanged){
            JOptionPane.showMessageDialog(null, "Password Updated Successfully ", "Success ", JOptionPane.INFORMATION_MESSAGE);

        }
         else{
             JOptionPane.showMessageDialog(null, "No changes Done ", "Message", JOptionPane.INFORMATION_MESSAGE);

         }
    
}
    

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PtEditProfileFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PtEditProfileFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PtEditProfileFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PtEditProfileFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PtEditProfileFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox chkPassword;
    private javax.swing.JCheckBox chkResume;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JPanel passwordPanel;
    private javax.swing.JPanel resumePanel;
    private javax.swing.JButton txtCfrmPwd;
    private javax.swing.JButton txtEmail;
    private javax.swing.JButton txtName;
    private javax.swing.JButton txtNewPwd;
    private javax.swing.JButton txtPhone;
    private javax.swing.JTextField txtResume;
    private javax.swing.JButton txtcurrentPwd;
    // End of variables declaration//GEN-END:variables
}
