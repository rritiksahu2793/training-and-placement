/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trandpl.gui;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import trandpl.dao.HrDAO;
import trandpl.dao.JobDAO;
import trandpl.pojo.CurrentUser;
import trandpl.pojo.JobPojo;

/**
 *
 * @author rriti
 */
public class HrEditJobFrame extends javax.swing.JFrame {

    /**
     * Creates new form HrEditJobFrame
     */
    private JobPojo job;

    public HrEditJobFrame() {
        initComponents();
        this.setLocationRelativeTo(null);
        lblName.setText(CurrentUser.getName());
    }

    public HrEditJobFrame(JobPojo job) {
        this();
        this.job = job;
        showJobDetails();

    }

    private void showJobDetails() {
        String companyName = "";
        try {
            companyName = HrDAO.getCompanyNameById(job.getHrId());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "DB Error ", "ERROR ", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
        lblJobId.setText(job.getJobId());
        txtCompanyName.setText(companyName);
        txtJobTitle.setText(job.getJobTitle());
        String [] allSkills = job.getTags().split(",");
        
        txtSkill1.setText(allSkills[0]);
        txtSkill2.setText(allSkills[1]);
        txtSkill3.setText(allSkills[2]);
        txtSkill4.setText(allSkills[3]);
        


        
        
        
    }
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator2 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblJobId = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtJobTitle = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtCompanyName = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        txtSkill1 = new javax.swing.JTextField();
        txtSkill2 = new javax.swing.JTextField();
        txtSkill3 = new javax.swing.JTextField();
        txtSkill4 = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(243, 246, 254));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/World-Map.png"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -20, -1, 200));

        jLabel2.setFont(new java.awt.Font("STXinwei", 1, 24)); // NOI18N
        jLabel2.setText("Welcome , ");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 150, 30));

        lblName.setFont(new java.awt.Font("STXinwei", 1, 24)); // NOI18N
        jPanel1.add(lblName, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 190, 160, 30));

        jLabel3.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel3.setText("JobId :- ");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, -1, -1));

        lblJobId.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        lblJobId.setText("jLabel4");
        jPanel1.add(lblJobId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 240, -1, -1));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, 960, -1));

        jLabel4.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel4.setText("Job Title ");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, -1, -1));

        jLabel5.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel5.setText("CompanyName");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 290, -1, -1));

        txtJobTitle.setBackground(new java.awt.Color(173, 192, 234));
        txtJobTitle.setBorder(null);
        jPanel1.add(txtJobTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 330, 240, 30));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, -1, -1));

        txtCompanyName.setBackground(new java.awt.Color(173, 192, 234));
        txtCompanyName.setBorder(null);
        jPanel1.add(txtCompanyName, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 330, 250, 30));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 320, -1, -1));

        jLabel8.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel8.setText("Skills Required :-");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, 160, 30));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 412, 960, -1));

        txtSkill1.setBackground(new java.awt.Color(173, 192, 234));
        txtSkill1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSkill1ActionPerformed(evt);
            }
        });
        jPanel1.add(txtSkill1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 432, 240, 30));

        txtSkill2.setBackground(new java.awt.Color(173, 192, 234));
        jPanel1.add(txtSkill2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 432, 260, 30));

        txtSkill3.setBackground(new java.awt.Color(173, 192, 234));
        txtSkill3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSkill3ActionPerformed(evt);
            }
        });
        jPanel1.add(txtSkill3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 480, 240, 30));

        txtSkill4.setBackground(new java.awt.Color(173, 192, 234));
        txtSkill4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSkill4ActionPerformed(evt);
            }
        });
        jPanel1.add(txtSkill4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 480, 260, 30));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 520, 940, -1));

        jButton1.setText("Logout");
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 550, -1, -1));

        jButton2.setText("Save Changes");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 550, -1, -1));

        jButton3.setText("Back");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 550, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 980, 600));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtSkill4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSkill4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSkill4ActionPerformed

    private void txtSkill1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSkill1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSkill1ActionPerformed

    private void txtSkill3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSkill3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSkill3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        
       if(validateInputs() == false){
           JOptionPane.showMessageDialog(null,"Please fill All the fields");
           return;
       }
       
       job.setJobTitle(txtJobTitle.getText());
       job.setTags(txtSkill1.getText().trim()+","+ txtSkill2.getText().trim()+","+txtSkill3.getText().trim()+","+txtSkill4.getText().trim());
       
       try{
         boolean result = JobDAO.editJobByJobId(job); 
         if(result){
             JOptionPane.showMessageDialog(null,"Job Added Successfully ", "Success",JOptionPane.INFORMATION_MESSAGE);
             return;
         }
         JOptionPane.showMessageDialog(null,"Job not Added ", "Sorry",JOptionPane.ERROR_MESSAGE);
       }
       
       catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "DB Error ", "ERROR ", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
       
       
       
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        new HrOptionsFrame().setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HrEditJobFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HrEditJobFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HrEditJobFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HrEditJobFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HrEditJobFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lblJobId;
    private javax.swing.JLabel lblName;
    private javax.swing.JTextField txtCompanyName;
    private javax.swing.JTextField txtJobTitle;
    private javax.swing.JTextField txtSkill1;
    private javax.swing.JTextField txtSkill2;
    private javax.swing.JTextField txtSkill3;
    private javax.swing.JTextField txtSkill4;
    // End of variables declaration//GEN-END:variables

   private boolean validateInputs(){
        if(txtJobTitle.getText().trim().isEmpty()||txtSkill1.getText().trim().isEmpty()||txtSkill2.getText().trim().isEmpty()||txtSkill3.getText().trim().isEmpty()||txtSkill4.getText().trim().isEmpty())
           return false;
        return true;
    }
}
