/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trandpl.gui;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;
import trandpl.pojo.CurrentUser;
import javax.swing.table.DefaultTableModel;
import trandpl.dao.HrDAO;
import trandpl.dao.UserDAO;
import trandpl.pojo.HrPojo;
/**
 *
 * @author rriti
 */
public class AdminRemoveHrFrame extends javax.swing.JFrame {

    /**
     * Creates new form AdminRemoveHrFrame
     */
    DefaultTableModel  model;
    
    public AdminRemoveHrFrame() {
        initComponents();
        lblName.setText(CurrentUser.getName());
        this.setLocationRelativeTo(null);
        model = (DefaultTableModel)tblAllHr.getModel();
        loadHrDetails();
        
    }

   private void loadHrDetails() {
         try{
            List <HrPojo> allHrList = HrDAO.getAllHr();
            for(HrPojo hr:allHrList){
                Vector <String> row = new Vector<>();
                row.add(hr.getHrId());
                row.add(hr.getUserId());
                row.add(hr.getHrName());
                row.add(hr.getPhone());
                row.add(hr.getCompanyName());
                
                model.addRow(row);
                
                
            }
            if(allHrList.isEmpty()){
                JOptionPane.showMessageDialog(null,"No Hr present in the table", "Hr not Present", JOptionPane.INFORMATION_MESSAGE);
                //return;
            }
            
            
         }
         catch(SQLException ex){
             JOptionPane.showMessageDialog(null,"Error in connection to DB","DB Error",JOptionPane.ERROR_MESSAGE);
             ex.printStackTrace();
             
         }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAllHr = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        btnRemoveHr = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(243, 246, 254));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/World-Map.png"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -40, 980, 230));

        tblAllHr.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Hr ID", "User Id", "Name", "Phone No", "Company Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblAllHr);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 210, 680, 300));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Remove-Emp.png"))); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, 190, -1));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, 950, -1));

        jLabel3.setFont(new java.awt.Font("STXinwei", 1, 24)); // NOI18N
        jLabel3.setText(" Welcome , ");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 180, 140, 30));

        lblName.setFont(new java.awt.Font("STXinwei", 1, 24)); // NOI18N
        jPanel1.add(lblName, new org.netbeans.lib.awtextra.AbsoluteConstraints(131, 180, 150, 30));

        jButton1.setText("Logout");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 560, -1, -1));

        btnRemoveHr.setText("Remove Selected Hr");
        btnRemoveHr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveHrActionPerformed(evt);
            }
        });
        jPanel1.add(btnRemoveHr, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 560, -1, -1));

        jButton3.setText("Back");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 560, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 980, 610));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRemoveHrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveHrActionPerformed
        // TODO add your handling code here:
        int row = tblAllHr.getSelectedRow();
        if(row==-1){
            JOptionPane.showMessageDialog(null,"Please select any First ");
            return;
        }
        String hrId = (String)tblAllHr.getValueAt(row, 0);
        int resp =JOptionPane.showConfirmDialog(null, "Are You sure you want to remove"+hrId+"?","Conformation",JOptionPane.OK_CANCEL_OPTION,JOptionPane.INFORMATION_MESSAGE);
        if(resp == JOptionPane.OK_OPTION){
            try{
                String userid = (String)tblAllHr.getValueAt(row,1);
                boolean result = UserDAO.removeUser(userid);
                if(result==true){
                    JOptionPane.showMessageDialog(null,"Record deleted successfully","Success",JOptionPane.INFORMATION_MESSAGE);
                    model.removeRow(row);
                    if(model.getRowCount()==0){
                        btnRemoveHr.setEnabled(false);
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null,"cannot Delete the Record","Deletion failed",JOptionPane.INFORMATION_MESSAGE);
                }
            }
             
            catch(SQLException ex){
             JOptionPane.showMessageDialog(null,"Error in connection to DB","DB Error",JOptionPane.ERROR_MESSAGE);
             ex.printStackTrace();
             
            }
            
        }
    }//GEN-LAST:event_btnRemoveHrActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        new AdminHrModuleframe().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        new LoginFame().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AdminRemoveHrFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AdminRemoveHrFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AdminRemoveHrFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AdminRemoveHrFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AdminRemoveHrFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRemoveHr;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblName;
    private javax.swing.JTable tblAllHr;
    // End of variables declaration//GEN-END:variables
}
