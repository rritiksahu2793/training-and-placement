/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trandpl.gui;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import trandpl.dao.JobDAO;
import trandpl.dao.QuestionDAO;
import trandpl.pojo.QuestionPojo;

/**
 *
 * @author rriti
 */
public class HrSetQuizFrame extends javax.swing.JFrame {

    private int counter = 0;
    private String jobId;
    private List<QuestionPojo> questionList = new ArrayList<>();

    public HrSetQuizFrame() {
        initComponents();
        this.setLocationRelativeTo(null);
        lblQno.setText(String.valueOf(counter + 1));

    }

    public HrSetQuizFrame(String jobId) {
        this();
        this.jobId = jobId;
        System.out.println(jobId);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtQuestion = new javax.swing.JTextPane();
        txtOption1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtOption2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtOption3 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtOption4 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cmbCorrectOption = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        btnPrevious = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        lblQno = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/World-Map.png"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, -20, -1, 200));

        jLabel2.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel2.setText(" QUES NO :-");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 110, 30));

        txtQuestion.setBackground(new java.awt.Color(173, 192, 234));
        jScrollPane1.setViewportView(txtQuestion);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 190, 840, 90));

        txtOption1.setBackground(new java.awt.Color(173, 192, 234));
        txtOption1.setBorder(null);
        jPanel1.add(txtOption1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 370, 250, 30));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 360, -1, -1));

        txtOption2.setBackground(new java.awt.Color(173, 192, 234));
        txtOption2.setBorder(null);
        jPanel1.add(txtOption2, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 370, 250, 30));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 360, -1, -1));

        txtOption3.setBackground(new java.awt.Color(173, 192, 234));
        txtOption3.setBorder(null);
        jPanel1.add(txtOption3, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 450, 250, 30));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 440, -1, -1));

        txtOption4.setBackground(new java.awt.Color(173, 192, 234));
        txtOption4.setBorder(null);
        jPanel1.add(txtOption4, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 450, 250, 30));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 440, -1, -1));

        jLabel7.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel7.setText("OPTIONS :-");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, 120, 30));

        jLabel8.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel8.setText("CORRECT OPTION :-");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 510, 210, 30));

        cmbCorrectOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Options", "Option 1", "Option 2", "Option 3", "Option 4" }));
        jPanel1.add(cmbCorrectOption, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 510, 250, 30));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, 1000, -1));

        jLabel9.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel9.setText("Option 1");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 380, -1, -1));

        jLabel10.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel10.setText("Option 2");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 370, -1, -1));

        jLabel11.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel11.setText("Option 4");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 440, -1, -1));

        jLabel12.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel12.setText("Option 3");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 450, -1, -1));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 340, -1, -1));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 330, 990, -1));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 560, 990, 10));

        btnPrevious.setText("Previous");
        btnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousActionPerformed(evt);
            }
        });
        jPanel1.add(btnPrevious, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 585, 110, 30));

        btnNext.setText("Next");
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });
        jPanel1.add(btnNext, new org.netbeans.lib.awtextra.AbsoluteConstraints(197, 585, 90, 30));

        jButton3.setText("Discard Paper");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(741, 580, 130, 30));

        jButton4.setText("Summit");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 580, 100, 30));

        lblQno.setFont(new java.awt.Font("STXinwei", 1, 24)); // NOI18N
        jPanel1.add(lblQno, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 220, 70, 50));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1010, 640));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        new HrQuizOptionFrame().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void getQuestionFromList() {
        QuestionPojo question;
        question = questionList.get(counter);
        txtQuestion.setText(question.getQuestion());
        txtOption1.setText(question.getOption1());
        txtOption2.setText(question.getOption2());
        txtOption3.setText(question.getOption3());
        txtOption4.setText(question.getOption4());
        cmbCorrectOption.setSelectedIndex(question.getCorrectOption());

    }


    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed

        if (validateInputs()) {
            QuestionPojo question = new QuestionPojo();
            if (counter == questionList.size()) {
                questionList.add(question);
                clearText();
                ++counter;
            } else {
                questionList.set(counter, question);
                ++counter;
                if (counter == questionList.size()) {
                    clearText();
                } else {
                    getQuestionFromList();
                }
            }
            lblQno.setText(String.valueOf(counter + 1));

        } else {
            JOptionPane.showMessageDialog(null, "Please all fill the fields first ", "ERROR", JOptionPane.INFORMATION_MESSAGE);
        }

    }//GEN-LAST:event_btnNextActionPerformed

    private void btnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousActionPerformed
        // TODO add your handling code here:
        if (counter == 0) {
            return;
        }

        if (counter == questionList.size()) {
            if (validateInputs()) {
                int result = JOptionPane.showConfirmDialog(null, "You have an unsaved Question . Do You want to save it ?", "Confirm ?", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    QuestionPojo question = setQuestion();
                    questionList.add(question);
                    clearText();
                }
            }
                --counter;
                getQuestionFromList();
            } else {
                if (validateInputs()) {
                    QuestionPojo question = setQuestion();
                    questionList.set(counter, question);
                    counter--;
                    getQuestionFromList();
                } else {
                    JOptionPane.showMessageDialog(null, "Please all fill the fields first ", "ERROR", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        
             lblQno.setText(String.valueOf(counter + 1));

    }//GEN-LAST:event_btnPreviousActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        
        if(validateInputs()){
            QuestionPojo que = setQuestion();
            if(counter == questionList.size()){
             questionList.add(que);
            }
            else{
                questionList.set(counter, que);
            }
            try{
                 boolean result = QuestionDAO.setPaper(questionList);
                    if(result){
                        JOptionPane.showMessageDialog(null, "Paper Added successfully", "Paper Added", JOptionPane.INFORMATION_MESSAGE);
                        JobDAO.setJobStatus(jobId);
                        new HrQuizOptionFrame().setVisible(true);
                        this.dispose();
                    }
                    else{
                        JOptionPane.showMessageDialog(null, "Could not add the Paper ", "Failure!!", JOptionPane.WARNING_MESSAGE);
                    }
                }
            catch(SQLException ex){
                JOptionPane.showMessageDialog(null, "Failure in connectio to database", "DB Error", JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
                return;
            }
            
        }
        
        else{
            JOptionPane.showMessageDialog(null, "Please fill all the Fields ", "Empty Fields", JOptionPane.WARNING_MESSAGE);
        }
        
        
        
    }//GEN-LAST:event_jButton4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HrSetQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HrSetQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HrSetQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HrSetQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HrSetQuizFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPrevious;
    private javax.swing.JComboBox<String> cmbCorrectOption;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lblQno;
    private javax.swing.JTextField txtOption1;
    private javax.swing.JTextField txtOption2;
    private javax.swing.JTextField txtOption3;
    private javax.swing.JTextField txtOption4;
    private javax.swing.JTextPane txtQuestion;
    // End of variables declaration//GEN-END:variables

    private boolean validateInputs() {
        if (txtQuestion.getText().trim().isEmpty() || txtOption1.getText().trim().isEmpty() || txtOption2.getText().trim().isEmpty() || txtOption3.getText().trim().isEmpty() || txtOption4.getText().trim().isEmpty() || cmbCorrectOption.getSelectedItem() == "Options") {
            return false;
        }

        return true;
    }

    private void clearText() {
        txtQuestion.setText("");
        txtOption1.setText("");
        txtOption2.setText("");
        txtOption3.setText("");
        txtOption4.setText("");
        cmbCorrectOption.setSelectedIndex(0);
        txtQuestion.requestFocus();
    }

    private QuestionPojo setQuestion() {

        QuestionPojo question = new QuestionPojo();

        question.setJobId(jobId);
        question.setQno(counter + 1);
        question.setQuestion(txtQuestion.getText().trim());
        question.setOption1(txtOption1.getText().trim());
        question.setOption2(txtOption2.getText().trim());
        question.setOption3(txtOption3.getText().trim());
        question.setOption4(txtOption4.getText().trim());
        question.setCorrectOption(cmbCorrectOption.getSelectedIndex());

        return question;
    }
}
