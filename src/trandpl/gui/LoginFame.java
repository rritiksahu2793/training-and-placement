
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trandpl.gui;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import trandpl.dao.UserDAO;
import trandpl.pojo.CurrentUser;
import trandpl.pojo.UserPojo;

/**
 *
 * @author rriti
 */
public class LoginFame extends javax.swing.JFrame {

    private String userId;
    private String password;
    public LoginFame() {
        initComponents();
        this.setLocationRelativeTo(null);
    }
    
    private String validateUser(){
        if(jrbAdmin.isSelected()){
            return "Admin";
        }
        else if(jrbHr.isSelected()){
            return "Hr";
        }
        else if(jrbStudents.isSelected()){
            return "Pt";
        }
        else{
            return null;
        }
    }

    private boolean validateInputs(){
        userId = txtUserId.getText(); 
        char[] pwd = txtPassword.getPassword();
        if(userId.isEmpty()||pwd.length==0){
            return false;
        }
        
        password = String.valueOf(pwd);
        return true;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnLogin = new javax.swing.JButton();
        btnSingUp = new javax.swing.JButton();
        jrbAdmin = new javax.swing.JRadioButton();
        jrbHr = new javax.swing.JRadioButton();
        jrbStudents = new javax.swing.JRadioButton();
        jTextField1 = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        txtPassword = new javax.swing.JPasswordField();
        jLabel6 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtUserId = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(243, 246, 254));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Email or User ID");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Password");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Select User Type");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 410, -1, -1));

        btnLogin.setBackground(new java.awt.Color(243, 246, 254));
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        jPanel1.add(btnLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 570, -1, 30));

        btnSingUp.setBackground(new java.awt.Color(243, 246, 254));
        btnSingUp.setText("Sign up");
        btnSingUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSingUpActionPerformed(evt);
            }
        });
        jPanel1.add(btnSingUp, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 570, -1, 30));

        jrbAdmin.setBackground(new java.awt.Color(243, 246, 254));
        buttonGroup1.add(jrbAdmin);
        jrbAdmin.setText("Admin");
        jrbAdmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrbAdminActionPerformed(evt);
            }
        });
        jPanel1.add(jrbAdmin, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 450, -1, -1));

        jrbHr.setBackground(new java.awt.Color(243, 246, 254));
        buttonGroup1.add(jrbHr);
        jrbHr.setText("HR");
        jrbHr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrbHrActionPerformed(evt);
            }
        });
        jPanel1.add(jrbHr, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 450, -1, -1));

        jrbStudents.setBackground(new java.awt.Color(243, 246, 254));
        buttonGroup1.add(jrbStudents);
        jrbStudents.setText("Students");
        jrbStudents.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrbStudentsActionPerformed(evt);
            }
        });
        jPanel1.add(jrbStudents, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 450, -1, -1));

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        jPanel1.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 162, 328, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 380, 457, 10));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 510, 458, 10));

        txtPassword.setBackground(new java.awt.Color(173, 192, 234));
        txtPassword.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        txtPassword.setBorder(null);
        txtPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPasswordActionPerformed(evt);
            }
        });
        jPanel1.add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 280, 360, 30));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_4018958162.png"))); // NOI18N
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 270, -1, 40));
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 170, 100, 80));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/homePage.jpg"))); // NOI18N
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, -10, 560, 500));

        txtUserId.setBackground(new java.awt.Color(173, 192, 234));
        txtUserId.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        txtUserId.setBorder(null);
        txtUserId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUserIdActionPerformed(evt);
            }
        });
        jPanel1.add(txtUserId, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 140, 400, 20));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_4018958162.png"))); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 540, 40));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 706, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void txtPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPasswordActionPerformed

    private void jrbStudentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrbStudentsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jrbStudentsActionPerformed

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
       if(validateInputs()==false){
           JOptionPane.showMessageDialog(null,"Please fill all the fields","Error",JOptionPane.ERROR_MESSAGE);
           return;
       }
       String type = validateUser();
       if(validateUser()==null){
          JOptionPane.showMessageDialog(null,"Please select user type","Error",JOptionPane.ERROR_MESSAGE);
          return; 
       }
       
       UserPojo user = new UserPojo();
       user.setUserid(userId.toUpperCase());
       user.setPassword(password);
       user.setType(type);
       
       try{
           boolean result = UserDAO.validateUser(user);
           if(result==true){
               if(CurrentUser.getType().equals("Admin")){
                   JOptionPane.showMessageDialog(null,"Wellcome Admin "+CurrentUser.getName(),"Success",JOptionPane.INFORMATION_MESSAGE);
                   new AdminOptionsFrame().setVisible(true);
                   this.dispose();
                   
                }
               
               else if(CurrentUser.getType().equals("Hr")){
                   JOptionPane.showMessageDialog(null,"Wellcome Hr "+CurrentUser.getName(),"Success",JOptionPane.INFORMATION_MESSAGE);
                   new HrOptionsFrame().setVisible(true);
                   this.dispose();
                }
               
               else {
                   // students frame code 
                   JOptionPane.showMessageDialog(null,"Wellcome Student  "+CurrentUser.getName(),"Success",JOptionPane.INFORMATION_MESSAGE);
                   new ParticipantsOptionFrame().setVisible(true);
                   this.dispose();
                }
               
               
           }
           
           else{
               JOptionPane.showMessageDialog(null,"Wrong userid or password or type","Error",JOptionPane.ERROR_MESSAGE);
               return; 
               
           }
           
           
           
       }
       
       catch(SQLException ex){
           JOptionPane.showMessageDialog(null,"Error in connection to DB","Error",JOptionPane.ERROR_MESSAGE);
           ex.printStackTrace();
       }
       
       
    }//GEN-LAST:event_btnLoginActionPerformed

    private void jrbHrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrbHrActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jrbHrActionPerformed

    private void jrbAdminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrbAdminActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jrbAdminActionPerformed

    private void txtUserIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUserIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUserIdActionPerformed

    private void btnSingUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSingUpActionPerformed
        // TODO add your handling code here:
        new SignUpframe().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnSingUpActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginFame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginFame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginFame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginFame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginFame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnSingUp;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JRadioButton jrbAdmin;
    private javax.swing.JRadioButton jrbHr;
    private javax.swing.JRadioButton jrbStudents;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtUserId;
    // End of variables declaration//GEN-END:variables
}
