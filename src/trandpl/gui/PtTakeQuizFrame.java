/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trandpl.gui;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import trandpl.dao.QuestionDAO;
import trandpl.dao.ResultDAO;
import trandpl.pojo.CurrentUser;
import trandpl.pojo.QuestionPojo;
import trandpl.pojo.ResultsPojo;

/**
 *
 * @author rriti
 */
public class PtTakeQuizFrame extends javax.swing.JFrame {

    /**
     * Creates new form PtTakeQuizFrame
     */
    private String jobId;
    private String jobTitle;
    private int counter =0;
    private List<QuestionPojo> questionList;
    private List<Integer> answerList;
    private QuestionPojo currentQuestion; 
    
    public PtTakeQuizFrame() {
        initComponents();
        this.setLocationRelativeTo(null);
    }
    public PtTakeQuizFrame(String jobId , String jobTitle){
        this();
        this.jobId= jobId;
        this.jobTitle = jobTitle;
        lblTitle.setText(jobTitle);
        lblQuestionNo.setText(""+counter+1);
        loadQuestionPaper();
    }
    
    private void loadQuestionPaper(){
        try{
            questionList = QuestionDAO.getQuestionPaperByJobId(jobId);
            answerList = new ArrayList<>(questionList.size());
            for(int i=0; i<questionList.size();i++){
               answerList.add(i,0); 
            }
            showNextQuestionInFrame();
        }
        
        catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Error in Connection to DataBase", "DB Error ", JOptionPane.ERROR_MESSAGE );
            ex.printStackTrace();
            return;
            
        }
        
    }
    
    private void showNextQuestionInFrame(){
        currentQuestion = questionList.get(counter);
        txtQuestion.setText(currentQuestion.getQuestion());
        jrbOption1.setText(currentQuestion.getOption1());
        jrbOption2.setText(currentQuestion.getOption2());
        jrbOption3.setText(currentQuestion.getOption3());
        jrbOption4.setText(currentQuestion.getOption4());
        
        int answer = answerList.get(counter);
        switch(answer){
            case 1:
                jrbOption1.setSelected(true);
                break;
            case 2:
                jrbOption2.setSelected(true);
                break;
            case 3:
                jrbOption3.setSelected(true);
                break;
            case 4:
                jrbOption4.setSelected(true);
                break;
            default:
                buttonGroup1.clearSelection();
        }
        
    }
 
    private int validateChooseOption(){
        if(jrbOption1.isSelected())
            return 1;
        else if(jrbOption2.isSelected())
            return 2;
        else if(jrbOption3.isSelected())
            return 3;
        else if(jrbOption4.isSelected())
            return 4;
        
        else
            return 0;
    }
    
    private double getPercentage(){
        double markObtained=0;
        for(int i=0; i<questionList.size();i++){
            QuestionPojo question = questionList.get(i);
            if(question.getCorrectOption()==answerList.get(i)){
                markObtained++;
            }
        }
        
        double per = (markObtained/questionList.size())*100;
        
        return per;
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtQuestion = new javax.swing.JTextPane();
        jLabel1 = new javax.swing.JLabel();
        lblQuestionNo = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblTitle = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jrbOption2 = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jrbOption4 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jrbOption1 = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        jrbOption3 = new javax.swing.JRadioButton();
        jLabel8 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnSummit = new javax.swing.JButton();
        btnDiscard = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        btnPrevious = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/World-Map.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, -10, -1, 190));

        jScrollPane1.setViewportView(txtQuestion);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 250, 830, 80));

        jLabel1.setText("Q No");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 260, 60, 50));

        lblQuestionNo.setText("jLabel3");
        getContentPane().add(lblQuestionNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 260, 50, 50));

        jLabel4.setText("Paper for  :");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 190, 80, 40));
        getContentPane().add(lblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 190, 160, 40));

        jLabel3.setText("Options : -");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 360, 120, 30));

        buttonGroup1.add(jrbOption2);
        jrbOption2.setText("jRadioButton1");
        getContentPane().add(jrbOption2, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 410, 250, 30));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 400, 290, -1));

        buttonGroup1.add(jrbOption4);
        jrbOption4.setText("jRadioButton1");
        getContentPane().add(jrbOption4, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 470, 250, 30));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 460, 300, -1));

        buttonGroup1.add(jrbOption1);
        jrbOption1.setText("jRadioButton1");
        jrbOption1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrbOption1ActionPerformed(evt);
            }
        });
        getContentPane().add(jrbOption1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 410, 250, 30));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 400, 300, 50));

        buttonGroup1.add(jrbOption3);
        jrbOption3.setText("jRadioButton1");
        getContentPane().add(jrbOption3, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 470, 250, 30));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 460, 300, -1));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 520, 1000, 10));

        btnSummit.setText("Summit");
        btnSummit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSummitActionPerformed(evt);
            }
        });
        getContentPane().add(btnSummit, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 550, 140, 30));

        btnDiscard.setText("Discard");
        btnDiscard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDiscardActionPerformed(evt);
            }
        });
        getContentPane().add(btnDiscard, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 550, 130, 30));

        btnNext.setText("Next");
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });
        getContentPane().add(btnNext, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 550, 130, 30));

        btnPrevious.setText("Previous");
        btnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousActionPerformed(evt);
            }
        });
        getContentPane().add(btnPrevious, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 550, 130, 30));

        jLabel9.setText("Option 2");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 420, -1, -1));

        jLabel10.setText("Option 4");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 480, -1, -1));

        jLabel11.setText("Option 1");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 420, -1, -1));

        jLabel12.setText("Option 3");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 470, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jrbOption1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrbOption1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jrbOption1ActionPerformed

    private void btnDiscardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDiscardActionPerformed
        // TODO add your handling code here:
        new ParticipantsOptionFrame().setVisible(true);
    }//GEN-LAST:event_btnDiscardActionPerformed

    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
        // TODO add your handling code here:
        int res = validateChooseOption();
        if(res==0){
            JOptionPane.showMessageDialog(null ,"No Option Selected " , "Pls Select an Option", JOptionPane.WARNING_MESSAGE);
            return;
        }
        answerList.set(counter,res);
        counter++;
        if(counter==questionList.size()){
            counter =0;
        }
        showNextQuestionInFrame();
        lblQuestionNo.setText(""+counter+1);
        
    }//GEN-LAST:event_btnNextActionPerformed

    private void btnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousActionPerformed
        // TODO add your handling code here:
        int res = validateChooseOption();
        if(res==0){
            JOptionPane.showMessageDialog(null ,"No Option Selected " , "Pls Select an Option", JOptionPane.WARNING_MESSAGE);
            return;
        }
        answerList.set(counter,res);
        counter--;
        if(counter== -1){
            counter =questionList.size()-1;
        }
        showNextQuestionInFrame();
        lblQuestionNo.setText(""+counter+1);
    }//GEN-LAST:event_btnPreviousActionPerformed

    private void btnSummitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSummitActionPerformed
        // TODO add your handling code here:
        int res = validateChooseOption();
        if(res==0){
            JOptionPane.showMessageDialog(null ,"No Option Selected " , "Pls Select an Option", JOptionPane.WARNING_MESSAGE);
            return;
        }
        answerList.set(counter,res);
        double per = getPercentage();
        ResultsPojo result = new ResultsPojo();
        result.setJobId(jobId);
        result.setpId(CurrentUser.getId());
        if(per<5)
            per=5.0;
        result.setPercentage(per);
        result.setSelectedByHr("No");
        
        try{
            boolean resp = ResultDAO.setResult(result);
            if(resp){
              JOptionPane.showMessageDialog(null ,"you got" +per +"% marks and result has been sent to your email" , "Mark Obtained", JOptionPane.INFORMATION_MESSAGE);
              new ParticipantsOptionFrame().setVisible(true);
              this.dispose();
            }
            
        }
        catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Error in Connection to DataBase", "DB Error ", JOptionPane.ERROR_MESSAGE );
            ex.printStackTrace();
            return;
            
        }
    }//GEN-LAST:event_btnSummitActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PtTakeQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PtTakeQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PtTakeQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PtTakeQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PtTakeQuizFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDiscard;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPrevious;
    private javax.swing.JButton btnSummit;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton jrbOption1;
    private javax.swing.JRadioButton jrbOption2;
    private javax.swing.JRadioButton jrbOption3;
    private javax.swing.JRadioButton jrbOption4;
    private javax.swing.JLabel lblQuestionNo;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JTextPane txtQuestion;
    // End of variables declaration//GEN-END:variables
}
