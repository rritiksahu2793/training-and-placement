/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trandpl.gui;

import java.sql.SQLException;
import java.util.List;
import javax.swing.JOptionPane;
import trandpl.dao.QuestionDAO;
import trandpl.pojo.QuestionPojo;

/**
 *
 * @author rriti
 */
public class HrEditQuizFrame extends javax.swing.JFrame {

    /**
     * Creates new form HrEditQuizFrame
     */
    private int counter = 0 ;
    private String jobId;
    private List<QuestionPojo> questionList;
    private QuestionPojo currentQuestion;
    public HrEditQuizFrame() {
        initComponents();
        this.setLocationRelativeTo(null);
        
        
    }
    private void setQuestionInList(){
        currentQuestion.setQuestion(txtQuestion.getText().trim());
        currentQuestion.setOption1(txtOption1.getText().trim());
        currentQuestion.setOption2(txtOption2.getText().trim());
        currentQuestion.setOption3(txtOption3.getText().trim());
        currentQuestion.setOption4(txtOption4.getText().trim());
        currentQuestion.setCorrectOption(cmCorrectOption.getSelectedIndex());
        questionList.set(counter, currentQuestion);
        
        
    }
    
    public HrEditQuizFrame(String jobId) {
        this();
        this.jobId= jobId;
        lblQno.setText(""+ (counter+1));
        loadQuestionPaper();
    }
    private boolean validateInputs(){
        if(txtQuestion.getText().trim().isEmpty()||txtOption1.getText().trim().isEmpty()||txtOption2.getText().trim().isEmpty()||txtOption3.getText().trim().isEmpty()||txtOption4.getText().trim().isEmpty()||cmCorrectOption.getSelectedIndex() == -1)
            return false;
        return true;
            }
    
    private void loadQuestionPaper(){
        try{
          questionList = QuestionDAO.getQuestionPaperByJobId(jobId);
          showNextQuestionInFrame();
        }
        catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "Error in Connection to DataBase", "DB ERROR", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
    }

   
    
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator3 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblQno = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        txtOption1 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtOption2 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtOption3 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtOption4 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        cmCorrectOption = new javax.swing.JComboBox<>();
        jSeparator4 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtQuestion = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(243, 246, 254));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/World-Map.png"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, -10, -1, 200));

        jLabel2.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel2.setText("Q No");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 220, 80, 30));

        lblQno.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jPanel1.add(lblQno, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 260, 80, 30));

        jLabel4.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel4.setText("Options : - ");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 326, 130, 30));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 980, -1));

        jLabel5.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel5.setText("Option 1");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(48, 386, 80, 30));

        txtOption1.setBackground(new java.awt.Color(173, 192, 234));
        txtOption1.setBorder(null);
        jPanel1.add(txtOption1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 390, 250, 30));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 380, -1, -1));

        txtOption2.setBackground(new java.awt.Color(173, 192, 234));
        txtOption2.setBorder(null);
        jPanel1.add(txtOption2, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 390, 250, 30));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 380, -1, -1));

        txtOption3.setBackground(new java.awt.Color(173, 192, 234));
        txtOption3.setBorder(null);
        jPanel1.add(txtOption3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 460, 240, 30));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 450, -1, -1));

        txtOption4.setBackground(new java.awt.Color(173, 192, 234));
        txtOption4.setBorder(null);
        txtOption4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOption4ActionPerformed(evt);
            }
        });
        jPanel1.add(txtOption4, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 460, 250, 30));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tg_image_915734271.png"))); // NOI18N
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 450, -1, -1));

        jLabel10.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel10.setText("Option 2");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(48, 446, 80, 40));

        jLabel11.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel11.setText("Option 3");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 390, 90, 30));

        jLabel12.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel12.setText("Option 4");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 460, 90, 30));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 500, 990, -1));

        jLabel13.setFont(new java.awt.Font("STXinwei", 1, 18)); // NOI18N
        jLabel13.setText("Correct Option :");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 520, 150, 30));

        cmCorrectOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Options", "Option 1", "Option 2", "Option 3", "Option 4", " " }));
        jPanel1.add(cmCorrectOption, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 520, 280, 30));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 570, 1000, -1));

        jButton1.setText("Previous");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 600, 120, -1));

        jButton2.setText("Next");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 600, 120, -1));

        jButton3.setText("Discard");
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 600, 120, -1));

        jButton4.setText("Summit");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 600, 120, -1));

        jScrollPane1.setViewportView(txtQuestion);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 220, 810, 90));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1000, 660));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtOption4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOption4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOption4ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if(validateInputs()){
            setQuestionInList();
            try{
                boolean result = QuestionDAO.editQuestionPaper(questionList);
                if(result){
                 JOptionPane.showMessageDialog(null, "Paper Updated Successfully", "Success", JOptionPane.INFORMATION_MESSAGE); 
                 new HrChooseJobForEditingQuizFrame().setVisible(true);
                 this.dispose();
                }
                else{
                  JOptionPane.showMessageDialog(null, "Sorry cannot Update Paper", "Sorry ", JOptionPane.ERROR_MESSAGE);   
                }
                
            }
            catch(SQLException ex){
                 JOptionPane.showMessageDialog(null, "Error in Connection to DataBase", "DB ERROR", JOptionPane.ERROR_MESSAGE);
                  ex.printStackTrace();
                  return;
            }
            
        }     
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(validateInputs()){
            setQuestionInList();
            counter++;
            if(counter == questionList.size()){
                counter =0;
            }
            lblQno.setText(""+ (counter+1));
            showNextQuestionInFrame();  
        }
        else{
            JOptionPane.showMessageDialog(null, "Please fill all The fields", "Empty fields ", JOptionPane.INFORMATION_MESSAGE);
        }
        
        
        
        
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        if(validateInputs()){
            setQuestionInList();
            counter--;
            if(counter ==-1){
                counter = questionList.size()-1;
            }
            lblQno.setText(" " + (counter+1));
            showNextQuestionInFrame();
        }
        
        else{
            JOptionPane.showMessageDialog(null, "Please fill all The fields", "Empty fields ", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HrEditQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HrEditQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HrEditQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HrEditQuizFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HrEditQuizFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cmCorrectOption;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lblQno;
    private javax.swing.JTextField txtOption1;
    private javax.swing.JTextField txtOption2;
    private javax.swing.JTextField txtOption3;
    private javax.swing.JTextField txtOption4;
    private javax.swing.JTextPane txtQuestion;
    // End of variables declaration//GEN-END:variables

    private void showNextQuestionInFrame() {
        currentQuestion = questionList.get(counter);
        txtQuestion.setText(currentQuestion.getQuestion());
        txtOption1.setText(currentQuestion.getOption1());
        txtOption2.setText(currentQuestion.getOption2());
        txtOption3.setText(currentQuestion.getOption3());
        txtOption4.setText(currentQuestion.getOption4());
        cmCorrectOption.setSelectedIndex(currentQuestion.getCorrectOption());

        
    }
}
